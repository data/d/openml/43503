# OpenML dataset: COVID19-cases-by-country

https://www.openml.org/d/43503

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
COVID19 is spreading across the globe and this data set help in analyzing to what extent the pandemic has affected different countries.
Content
This data set contains the total number of COVID 19 cases reported per country till 26 June 2020
There are 6 columns. 
date,    location,   totalcases,    newcases,  totaldeaths,   newdeaths
Acknowledgements
The data was collected from https://ourworldindata.org/coronavirus-data
Inspiration
Lets try to create a bar chart race from this data for the total cases reported by country.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43503) of an [OpenML dataset](https://www.openml.org/d/43503). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43503/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43503/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43503/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

